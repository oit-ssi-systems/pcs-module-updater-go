# PCS Module Updater (In Go!)

This is a go port of the
[pcs-module-updater](https://gitlab.oit.duke.edu/oit-ssi-systems/pcs-module-updater)
scripts. Hoping this can replace all the misc. python glue used to keep modules
up to date.

## Changes from Python Scripts

* All python scripting has been consolidated in to a single go binary called `pcs-module-updater`

* Instead of putting the modules to ignore in the script, these should now go in a new section of the Puppetfile.yaml file like so:

```yaml
duke_modules:
...
upstream_modules:
...
ignore_updates:
  - "puppetlabs/apache"
```

* Vault authentication has been removed from the scripts, and should be set up before running it. You'll need VAULT_ADDR and VAULT_TOKEN set up before running anything

## Commands

### patch

This command will submit an MR for any outdated modules in a project. Use it
with:

```bash
$ pcs-module-updater oit-ssi-systems/pcs
...
```

### outdated

This will just print the outdated modules from a given Puppetfile.yaml

```bash
$ pcs-module-updater outdated ../../drews/fake-pcs-modules/Puppetfile.yaml
...
```

### generate-puppetfile

Generate the standard Puppetfile from a Puppetfile.yaml. By default, it will
look for Puppetfile.yaml in your current directory.

```bash
$ pcs-module-updater generate-puppetfile > Puppetfile
...
```
