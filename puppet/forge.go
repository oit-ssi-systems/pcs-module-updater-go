package puppet

import (
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"slices"
	"strings"

	version "github.com/hashicorp/go-version"
)

// ForgeClient is just a simple client for connecting to puppet forge
type ForgeClient struct {
	BaseURL    string
	token      string
	httpClient *http.Client
}

// ForgeClientOpts are the options passed to a new module
type ForgeClientOpts struct {
	Token      string
	BaseURL    string
	HTTPClient *http.Client
}

func (c *ForgeClient) getModule(slug string) (*ForgeModuleResponse, error) {
	slug = strings.ReplaceAll(slug, "/", "-")
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", c.BaseURL, "/v3/modules/"+slug), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+c.token)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var fmr *ForgeModuleResponse
	err = json.Unmarshal(body, &fmr)
	if err != nil {
		return nil, err
	}
	return fmr, nil
}

// CurrentVersion returns the current version of a module
func (c *ForgeClient) CurrentVersion(slug string) (*version.Version, error) {
	data, err := c.getModule(slug)
	if err != nil {
		return nil, err
	}
	v, err := version.NewSemver(data.CurrentRelease.Version)
	if err != nil {
		return nil, err
	}
	return v, nil
}

// OutdatedVersions returns outdated versions in a puppet file
func (c *ForgeClient) OutdatedVersions(pf *Puppetfile) (OutdatedModules, error) {
	var outdated OutdatedModules
	for _, item := range pf.UpstreamModules {
		upVers, err := c.CurrentVersion(item.Name)
		if err != nil {
			return nil, err
		}
		currentVersion, err := version.NewVersion(item.Version)
		if err != nil {
			return nil, err
		}
		if upVers.GreaterThan(currentVersion) {
			slog.Debug("evaluating", "item", item.Name)
			if slices.Contains(pf.IgnoreUpdates, item.Name) {
				slog.Info("ignoring module listed in ignore_modules", "module", item.Name, "current", item.Version, "upstream", upVers.String())
				continue
			}
			outdated = append(outdated, OutdatedModule{
				Slug:     item.Name,
				Current:  item.Version,
				Upstream: upVers.String(),
			})
		}
	}
	return outdated, nil
}

// NewForgeClient returns a new puppetforge client
func NewForgeClient(opts *ForgeClientOpts) *ForgeClient {
	c := &ForgeClient{}
	if opts.HTTPClient == nil {
		c.httpClient = http.DefaultClient
	} else {
		c.httpClient = opts.HTTPClient
	}
	c.token = opts.Token

	if opts.BaseURL != "" {
		c.BaseURL = opts.BaseURL
	} else {
		c.BaseURL = "https://forgeapi.puppet.com"
	}
	return c
}

// ForgeModuleResponse is what we get back from Puppetforge
type ForgeModuleResponse struct {
	CreatedAt      string `json:"created_at,omitempty"`
	CurrentRelease struct {
		Changelog   string      `json:"changelog,omitempty"`
		CreatedAt   string      `json:"created_at,omitempty"`
		DeletedAt   interface{} `json:"deleted_at,omitempty"`
		DeletedFor  interface{} `json:"deleted_for,omitempty"`
		Downloads   float64     `json:"downloads,omitempty"`
		FileMd5     string      `json:"file_md5,omitempty"`
		FileSha256  string      `json:"file_sha256,omitempty"`
		FileSize    float64     `json:"file_size,omitempty"`
		FileURI     string      `json:"file_uri,omitempty"`
		License     string      `json:"license,omitempty"`
		MalwareScan interface{} `json:"malware_scan,omitempty"`
		Metadata    struct {
			Author       string `json:"author,omitempty"`
			Dependencies []struct {
				Name               string `json:"name,omitempty"`
				VersionRequirement string `json:"version_requirement,omitempty"`
			} `json:"dependencies,omitempty"`
			IssuesURL              string `json:"issues_url,omitempty"`
			License                string `json:"license,omitempty"`
			Name                   string `json:"name,omitempty"`
			OperatingsystemSupport []struct {
				Operatingsystem        string   `json:"operatingsystem,omitempty"`
				Operatingsystemrelease []string `json:"operatingsystemrelease,omitempty"`
			} `json:"operatingsystem_support,omitempty"`
			ProjectPage  string `json:"project_page,omitempty"`
			Requirements []struct {
				Name               string `json:"name,omitempty"`
				VersionRequirement string `json:"version_requirement,omitempty"`
			} `json:"requirements,omitempty"`
			Source  string   `json:"source,omitempty"`
			Summary string   `json:"summary,omitempty"`
			Tags    []string `json:"tags,omitempty"`
			Version string   `json:"version,omitempty"`
		} `json:"metadata,omitempty"`
		Module struct {
			DeprecatedAt interface{} `json:"deprecated_at,omitempty"`
			Name         string      `json:"name,omitempty"`
			Owner        struct {
				GravatarID string `json:"gravatar_id,omitempty"`
				Slug       string `json:"slug,omitempty"`
				URI        string `json:"uri,omitempty"`
				Username   string `json:"username,omitempty"`
			} `json:"owner,omitempty"`
			Slug string `json:"slug,omitempty"`
			URI  string `json:"uri,omitempty"`
		} `json:"module,omitempty"`
		Pdk             bool          `json:"pdk,omitempty"`
		Plans           []interface{} `json:"plans,omitempty"`
		Readme          string        `json:"readme,omitempty"`
		Reference       string        `json:"reference,omitempty"`
		Slug            string        `json:"slug,omitempty"`
		Supported       bool          `json:"supported,omitempty"`
		Tags            []string      `json:"tags,omitempty"`
		Tasks           []interface{} `json:"tasks,omitempty"`
		UpdatedAt       string        `json:"updated_at,omitempty"`
		URI             string        `json:"uri,omitempty"`
		ValidationScore float64       `json:"validation_score,omitempty"`
		Version         string        `json:"version,omitempty"`
	} `json:"current_release,omitempty"`
	DeprecatedAt  interface{} `json:"deprecated_at,omitempty"`
	DeprecatedFor interface{} `json:"deprecated_for,omitempty"`
	Downloads     float64     `json:"downloads,omitempty"`
	Endorsement   interface{} `json:"endorsement,omitempty"`
	FeedbackScore interface{} `json:"feedback_score,omitempty"`
	HomepageURL   string      `json:"homepage_url,omitempty"`
	IssuesURL     string      `json:"issues_url,omitempty"`
	ModuleGroup   string      `json:"module_group,omitempty"`
	Name          string      `json:"name,omitempty"`
	Owner         struct {
		GravatarID string `json:"gravatar_id,omitempty"`
		Slug       string `json:"slug,omitempty"`
		URI        string `json:"uri,omitempty"`
		Username   string `json:"username,omitempty"`
	} `json:"owner,omitempty"`
	Premium  bool `json:"premium,omitempty"`
	Releases []struct {
		CreatedAt string      `json:"created_at,omitempty"`
		DeletedAt interface{} `json:"deleted_at,omitempty"`
		FileSize  float64     `json:"file_size,omitempty"`
		FileURI   string      `json:"file_uri,omitempty"`
		Slug      string      `json:"slug,omitempty"`
		Supported bool        `json:"supported,omitempty"`
		URI       string      `json:"uri,omitempty"`
		Version   string      `json:"version,omitempty"`
	} `json:"releases,omitempty"`
	Slug         string      `json:"slug,omitempty"`
	SupersededBy interface{} `json:"superseded_by,omitempty"`
	Supported    bool        `json:"supported,omitempty"`
	UpdatedAt    string      `json:"updated_at,omitempty"`
	URI          string      `json:"uri,omitempty"`
}
