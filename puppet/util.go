package puppet

import (
	"fmt"
	"io"
	"os"
)

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprint(os.Stderr, "error closing item\n")
	}
}
