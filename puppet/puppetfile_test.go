package puppet

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMergePuppetfileOutdated(t *testing.T) {
	og := Puppetfile{
		UpstreamModules: []*Module{
			{
				Name:    "foo",
				Version: "0.1.0",
			},
			{
				Name:    "bar",
				Version: "0.0.1",
			},
		},
	}
	require.NoError(t, og.MergeOutdated(OutdatedModules{
		{
			Slug:     "foo",
			Upstream: "0.1.1",
		},
	}))
	require.Equal(t, Puppetfile{
		UpstreamModules: []*Module{
			{
				Name:    "foo",
				Version: "0.1.1",
			},
			{
				Name:    "bar",
				Version: "0.0.1",
			},
		},
	}, og)
}
