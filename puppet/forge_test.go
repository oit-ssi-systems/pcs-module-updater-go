package puppet

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewForgeClient(t *testing.T) {
	require.NotNil(t, NewForgeClient(&ForgeClientOpts{}))
}

func TestForgeGetModule(t *testing.T) {
	expected, err := os.ReadFile("./testdata/module-response.json")
	require.NoError(t, err)
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprint(w, string(expected))
	}))
	defer svr.Close()
	client := NewForgeClient(&ForgeClientOpts{
		BaseURL: svr.URL,
	})
	got, err := client.getModule("puppet/snmp")
	require.NoError(t, err)
	require.NotNil(t, got)

	vers, err := client.CurrentVersion("puppet/snmp")
	require.NoError(t, err)
	require.Equal(t, "6.0.0", vers.String())
}
