/*
Package puppet holds information about puppet interactions
*/
package puppet

// Module is just the base info on a puppet module
type Module struct {
	Name    string `yaml:"name"`
	Version string `yaml:"version,omitempty"`
}

// Puppetfile represents a puppetfile
type Puppetfile struct {
	DukeModules     []Module  `yaml:"duke_modules"`
	UpstreamModules []*Module `yaml:"upstream_modules"`
	IgnoreUpdates   []string  `yaml:"ignore_updates"`
}

// OutdatedModule is everything about a module needed to figure out if it's updated
type OutdatedModule struct {
	Slug     string `yaml:"slug"`
	Current  string `yaml:"current"`
	Upstream string `yaml:"upstream"`
}

// OutdatedModules are multiple OutdatedModule objects
type OutdatedModules []OutdatedModule

// MergeOutdated merges the outdated bits in to a puppetfile
func (p *Puppetfile) MergeOutdated(outdated OutdatedModules) error {
	for _, mod := range p.UpstreamModules {
		for _, outd := range outdated {
			if mod.Name == outd.Slug {
				mod.Version = outd.Upstream
			}
		}
	}
	return nil
}
