package cmd

import (
	"bytes"
	"fmt"
	"html/template"
	"os"

	vault "github.com/hashicorp/vault/api"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"gitlab.oit.duke.edu/oit-ssi-systems/pcs-module-updater-go/puppet"
	"gopkg.in/yaml.v3"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

// patchCmd represents the patch command
var patchCmd = &cobra.Command{
	Use:   "patch GITLAB_PROJECT",
	Short: "Submit a patch to update puppet modules from a repository",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		vaultS := mustGetCmd[string](cmd, "git-user-auth-path")
		gitlabI := mustGetCmd[string](cmd, "gitlab-instance")
		testingBranch := mustGetCmd[string](cmd, "testing-branch")
		targetBranch := mustGetCmd[string](cmd, "target-branch")
		mrTitle := mustGetCmd[string](cmd, "mr-title")
		vc, err := vault.NewClient(vault.DefaultConfig())
		cobra.CheckErr(err)

		// Quick lookup
		projectName := args[0]

		// Look up gitlab auth
		gitAuth, err := vc.Logical().Read(vaultS)
		cobra.CheckErr(err)

		// Create a new GitLab client
		// glc, err := gitlab.NewClient(gitAuth.Data["data"].(map[string]interface{})["gitlab_token"].(string), gitlab.WithBaseURL(fmt.Sprintf("https://%v/api/v4", gitlabI)))
		glt := fetchGitlabToken()
		if glt == "" {
			logger.Warn("Could not find gitlab token in environment, using a token from vault instead")
			glt = gitAuth.Data["data"].(map[string]interface{})["gitlab_token"].(string)
		}
		glc, err := gitlab.NewClient(glt, gitlab.WithBaseURL(fmt.Sprintf("https://%v/api/v4", gitlabI)))
		cobra.CheckErr(err)

		logger.Info("looking up project", "project", projectName)
		project, _, err := glc.Projects.GetProject(projectName, &gitlab.GetProjectOptions{})
		cobra.CheckErr(err)

		logger.Info("found project", "project", project.NameWithNamespace)

		// See if we have an open MR already
		stateS := "opened"
		currentMRs, _, err := glc.MergeRequests.ListProjectMergeRequests(project.ID, &gitlab.ListProjectMergeRequestsOptions{
			State:  &stateS,
			Search: &mrTitle,
		})
		cobra.CheckErr(err)
		if len(currentMRs) > 0 {
			logger.Warn("Found existing MRs. Please close that one before trying another one", "url", currentMRs[0].WebURL)
			return
		}

		remoteRepo := fmt.Sprintf("https://%v/%v.git", gitlabI, projectName)
		// Create a temp directory to check the repo in to
		repoDir, err := os.MkdirTemp("", "")
		cobra.CheckErr(err)
		defer removeA(repoDir)
		logger.Info("about to clone repo", "dir", repoDir)
		gitRepoAuth := &http.BasicAuth{
			Username: gitAuth.Data["data"].(map[string]interface{})["username"].(string),
			Password: glt,
		}

		localRepo, err := git.PlainClone(repoDir, false, &git.CloneOptions{
			URL:      remoteRepo,
			Auth:     gitRepoAuth,
			Progress: os.Stderr,
		})
		cobra.CheckErr(err)

		// Clear out the remote branch if it already exists
		logger.Info("About to delete old spec if it exists")
		err = localRepo.Push(&git.PushOptions{
			Auth:     gitRepoAuth,
			RefSpecs: []config.RefSpec{config.RefSpec(":refs/heads/" + testingBranch)},
			Progress: os.Stdout,
		})
		if err != nil {
			logger.Warn("Error trying to delete old remote branch, probably ok to continue though", "error", err)
		}

		w, err := localRepo.Worktree()
		checkErr(err)
		branchRefName := plumbing.NewBranchReferenceName(testingBranch)
		err = w.Checkout(&git.CheckoutOptions{
			Create: true,
			Branch: branchRefName,
		})
		checkErr(err)

		err = os.Chdir(repoDir)
		checkErr(err)
		var pf puppet.Puppetfile
		pfb, err := os.ReadFile("./Puppetfile.yaml")
		checkErr(err)
		err = yaml.Unmarshal(pfb, &pf)
		checkErr(err)

		fc := puppet.NewForgeClient(&puppet.ForgeClientOpts{})
		outdated, err := fc.OutdatedVersions(&pf)
		checkErr(err)

		if len(outdated) == 0 {
			logger.Info("No outdated modules! 🥳")
			return
		}
		err = pf.MergeOutdated(outdated)
		checkErr(err)

		var yb bytes.Buffer
		yamlEncoder := yaml.NewEncoder(&yb)
		yamlEncoder.SetIndent(2)
		checkErr(yamlEncoder.Encode(pf))

		checkErr(os.WriteFile("Puppetfile.yaml", yb.Bytes(), 0o600))

		// git add ./Puppetfile.yaml
		_, err = w.Add("Puppetfile.yaml")
		checkErr(err)

		// git commit
		_, err = w.Commit("Updating module versions", &git.CommitOptions{
			Author: &object.Signature{
				Name:  "Gitlab Systems Robot 🤖",
				Email: "ssi-linux@duke.edu",
			},
		})
		checkErr(err)

		// git push
		logger.Info("About to create remote")
		_, err = localRepo.CreateRemote(&config.RemoteConfig{
			Name: testingBranch,
			URLs: []string{remoteRepo},
		})
		checkErr(err)
		logger.Info("About to push to remote")
		err = localRepo.Push(&git.PushOptions{
			Auth:       gitRepoAuth,
			RemoteName: testingBranch,
			Progress:   os.Stderr,
		})
		checkErr(err)

		logger.Info("creating merge request", "source", testingBranch, "target", targetBranch, "title", mrTitle)
		var mrBody bytes.Buffer
		tmpl, err := template.New("test").Parse(`|Module|Current Version|New Version|
|------|---------------|-----------|
{{- range . }}
|{{ .Slug }}|{{ .Current }}|{{ .Upstream }}|
{{- end }}
`)
		checkErr(err)
		err = tmpl.Execute(&mrBody, outdated)
		checkErr(err)
		mrBodyS := mrBody.String()

		mr, _, err := glc.MergeRequests.CreateMergeRequest(project.ID, &gitlab.CreateMergeRequestOptions{
			Title:        &mrTitle,
			SourceBranch: &testingBranch,
			TargetBranch: &targetBranch,
			Description:  &mrBodyS,
		})
		checkErr(err)
		logger.Info("Created new merge request", "url", mr.WebURL)
	},
}

func init() {
	rootCmd.AddCommand(patchCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	patchCmd.PersistentFlags().StringP("target-branch", "b", "development", "Branch to target MR to be merged in to")
	patchCmd.PersistentFlags().StringP("testing-branch", "t", "robot-feature-update-modules", "Branch to use for the MR")
	patchCmd.PersistentFlags().StringP("mr-title", "m", "Robot Generated Module Updates", "Title of the merge request")
	patchCmd.PersistentFlags().String("git-user-auth-path", "ssi-systems/kv/data/shared_service_accounts/gitlabssisystemsrobo", "Path to git username and password in vault")
	patchCmd.PersistentFlags().String("gitlab-instance", "gitlab.oit.duke.edu", "FQDN of the GitLab instance")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// patchCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func removeA(dir string) {
	if err := os.RemoveAll(dir); err != nil {
		logger.Error("error removing directory", "dir", dir)
	}
}
