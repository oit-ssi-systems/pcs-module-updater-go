package cmd

import (
	"bytes"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/pcs-module-updater-go/puppet"
	"gopkg.in/yaml.v3"
)

// generatePuppetfileCmd represents the generatePuppetfile command
var generatePuppetfileCmd = &cobra.Command{
	Use:   "generate-puppetfile",
	Short: "Generate a Puppetfile from a Puppetfile.yaml",
	RunE: func(cmd *cobra.Command, _ []string) error {
		pfy, err := cmd.Flags().GetString("puppetfile-yaml")
		if err != nil {
			return err
		}
		pfb, err := os.ReadFile(path.Clean(pfy))
		if err != nil {
			return err
		}

		var pf puppet.Puppetfile
		if err := yaml.Unmarshal(pfb, &pf); err != nil {
			return err
		}

		var out bytes.Buffer
		out.WriteString(fmt.Sprintf("## This was generated with %v, do not edit directly\n", os.Args[0]))
		for _, item := range pf.DukeModules {
			out.WriteString(fmt.Sprintf("mod '%v', :local => true\n", item.Name))
		}
		for _, item := range pf.UpstreamModules {
			out.WriteString(fmt.Sprintf("mod '%v', '%v'\n", item.Name, item.Version))
		}

		fmt.Println(strings.TrimSpace(out.String()))
		return nil
	},
}

func init() {
	rootCmd.AddCommand(generatePuppetfileCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	generatePuppetfileCmd.PersistentFlags().String("puppetfile-yaml", "./Puppetfile.yaml", "Location of the Puppetfile.yaml")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generatePuppetfileCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
