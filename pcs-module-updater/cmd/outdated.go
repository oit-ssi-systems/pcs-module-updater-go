package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/pcs-module-updater-go/puppet"

	"gopkg.in/yaml.v3"
)

// outdatedCmd represents the outdated command
var outdatedCmd = &cobra.Command{
	Use:   "outdated PUPPETFILE",
	Short: "Show outdated information from a Puppetfile.yaml",
	Args:  cobra.ExactArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		var pf puppet.Puppetfile
		b, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}
		if yerr := yaml.Unmarshal(b, &pf); yerr != nil {
			return yerr
		}

		fc := puppet.NewForgeClient(&puppet.ForgeClientOpts{})
		outdated, err := fc.OutdatedVersions(&pf)
		if err != nil {
			return err
		}
		out, err := yaml.Marshal(outdated)
		if err != nil {
			return err
		}
		fmt.Println(string(out))
		return nil
	},
}

func init() {
	rootCmd.AddCommand(outdatedCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// outdatedCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// outdatedCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
