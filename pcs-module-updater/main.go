/*
Package main executes the CLI
*/
package main

import "gitlab.oit.duke.edu/oit-ssi-systems/pcs-module-updater-go/pcs-module-updater/cmd"

func main() {
	cmd.Execute()
}
